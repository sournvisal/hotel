import Vue from 'vue'
import Router from 'vue-router'
import Masters from '@/components/Master'
import Login from '@/components/Login'

import Creator from '@/components/pages/Creator'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Masters',
      component: Masters,
      children: [
        {
          path: '/creator',
          name: 'Creator',
          component: Creator
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
